import { Component, OnInit } from "@angular/core";
import { Trader } from "../models/trader";
import { TwoMovingAverages } from "../models/two-moving-averages";
import { TraderService } from "../services/trader-service";
import { TraderUpdate } from "../services/trader-service";
import { CreateTraderComponent } from "./create-trader/create-trader.component";
import { MatDialog, MatDialogRef } from "@angular/material";

/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
@Component({
  selector: "trader-table",
  templateUrl: "./trader-table.html",
  styleUrls: ["./trader-table.css"]
})
export class TraderTable implements TraderUpdate, OnInit {

  ngOnInit(): void {
    this.service.getStocks()
      .subscribe((data: Array<string>) => {        
        this.stocks = data
      })
  }

  service: TraderService;
  dataSource: Array<Trader>;
  stocks

  openDialog () {
    const dialogRef = this.dialog.open(CreateTraderComponent, {
      data: {
        stocks: this.stocks
      }
    })
  }

  hasProfitableTrader(): boolean {
    if(this.dataSource.length == 0) {

      
      return false
    }
    for (let index = 0; index < this.dataSource.length; index++) {
      if(this.dataSource[index].profitOrLoss > 0) {
        return true;
      }
      
    }
    return false
  }

  /**
   * Store the injected service references.
   */
  constructor(service: TraderService, private dialog: MatDialog) {
    this.dataSource = []
    this.service = service;
    service.subscribe(this);
    service.notify();
  }

  /**
   * Replace our array of traders with the latest,
   * which will triger a UI update.
   */
  latestTraders(traders: Array<Trader>) {
    this.dataSource = traders;
  }

  /**
   * Helper to derive the total number of trades made by this trader.
   */
  getTotalTrades(): number {
    return this.dataSource.map(t => t.trades).reduce((x, y) => x + y, 0);
  }

  /**
   * Helper to derive the trader's total profit.
   */
  getTotalProfit(): number {
    return this.dataSource.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
  }
}
