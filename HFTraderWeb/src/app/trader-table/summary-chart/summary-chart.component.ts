import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { ChartType, ChartOptions } from "chart.js";
import { Label } from "ng2-charts";

@Component({
  selector: 'app-summary-chart',
  templateUrl: './summary-chart.component.html',
  styleUrls: ['./summary-chart.component.css']
})
export class SummaryChartComponent implements OnInit, OnChanges {
  ngOnChanges(): void {

    let tempTraders = []
    let tempProfits = []
    this.traders.forEach(element => {
      if(element.profitOrLoss > 0) {
        tempTraders.push([element["@type"], element.stock])
        tempProfits.push(element.profitOrLoss)
      }
    });

    this.pieChartLabels = tempTraders
    this.pieChartData = tempProfits
    this.pieChartLegend = true
    this.pieChartType = "pie"
    this.piechartColors = [
      {
        backgroundColor: this.generatePastelColors(tempProfits.length)
      }
    ]
  }

  @Input() traders

  generatePastelColors(num: number): string[] {

    let colorArr = []

    for(let i = 0; i < num; i++) {

      let colorStr = "hsl(" + 360 * Math.random() + ',' + (25 + 70 * Math.random()) + '%,' + (85 + 10 * Math.random()) + '%)'
      colorArr.push(colorStr)
    }

    return colorArr
  }

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'bottom'
    }
  }
  public pieChartLabels: Label[]
  public pieChartData: number[]
  public pieChartType: ChartType
  public pieChartLegend: boolean
  public piechartColors

  constructor() { }

  ngOnInit() {
  }

}
