import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTraderComponent } from './create-trader.component';
import { FormBuilder } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Inject } from '@angular/core';

describe('CreateTraderComponent', () => {

  beforeEach(function() {
    this.mockTraderService = jasmine.createSpyObj("", [ "createTrader" ]);
  });

it("calls the injected TraderService to create a new trader", function() {
    const toolbar = new CreateTraderComponent(this.mockTraderService, this.FormBuilder, this.MatDialog, this.MAT_DIALOG_DATA);
    toolbar.create();
    expect(this.mockTraderService.createTrader)
      .toHaveBeenCalledWith(jasmine.objectContaining({
          ID: 0,
          stock: "MRK",
          size: 1000,
          active: true,
          stopping: false,
          positions: [],
          profitOrLoss: 0,
          lengthShort: 30000,
          lengthLong: 60000,
          exitThreshold: 0.03
        }));
  });
});
