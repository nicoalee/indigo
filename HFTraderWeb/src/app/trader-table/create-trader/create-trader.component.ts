import { Component, OnInit, OnChanges, Inject } from "@angular/core";
import { Trader } from "../../models/trader";
import { TwoMovingAverages } from "../../models/two-moving-averages";
import { TraderService } from "../../services/trader-service";
import { BollingerBands } from 'src/app/models/bollinger-bands';
import { FormGroup, FormBuilder, Validators, ValidatorFn } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime } from "rxjs/operators";
import { PriceBreakout } from 'src/app/models/price-breakout';


@Component({
  selector: 'app-create-trader',
  templateUrl: './create-trader.component.html',
  styleUrls: ['./create-trader.component.css']
})
export class CreateTraderComponent implements OnInit {

  stocks: Array<string>
  createTraderOptions: FormGroup
  filteredOptions: Observable<string[]>
  service: TraderService;

  ngOnInit() {

    this.createTraderOptions = this.fb.group({
      // every trader
      type: "2MA",
      stock: "MRK",
      size: [1000, Validators.min(1)],
      exitThreshold: [3, Validators.min(1)],
      lengthShort: [30, Validators.required],
      // 2MA
      lengthLong: [60, Validators.required],

      // BB
      standardDevMult: [1, Validators.min(0.1)],
      numPeriods: [30, Validators.min(3)],

      }, {
      validator: this.validate
    })

    this.stocks = this.data.stocks

    this.filteredOptions = this.createTraderOptions.controls['stock'].valueChanges
      .pipe(
        startWith(""),
        debounceTime(200),
        map(value => this._filter(value))
      )
  }

  private _filter(value: string):string[] {    
    
    const filterValue = value.toLowerCase()
    return this.stocks.filter(option => option.toLowerCase().includes(filterValue))
  }

  trackStock(index, stock) {    
    return stock
  }


  /**
   * Set default values for all properties, which will flow out to the
   * initial UI via two-way binding.
   */
  constructor(
    service: TraderService, 
    private fb: FormBuilder, 
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any) {

    this.service = service;
  }

  validate: ValidatorFn = (group: FormGroup) => {
    const short = group.get('lengthShort').value
    const long = group.get('lengthLong').value

    return short < long && short > 0 ? null : {range: true}
  }

  closeDialog() {
    const dialog = this.dialog.closeAll();
  }

  onSubmit() {
    this.create()
    this.closeDialog()

    
  }

  /**
   * Reads the values of form controls via two-way binding.
   * Creates an instance of the trader (only 2MA traders currently supported)
   * and sends it to the server to be created and activated.
   */
  create() {
    const formResults = this.createTraderOptions.value
    
    switch (formResults.type) {
      case "2MA": {

        this.service.createTrader(new TwoMovingAverages
          (0, formResults.stock, formResults.size, true, false, [], 0, NaN,
            formResults.lengthShort * 1000, formResults.lengthLong * 1000, formResults.exitThreshold / 100));
        break;
      }
      
      case "BB": {

        this.service.createTrader(new BollingerBands
          (0, formResults.stock, formResults.size, true, false, [], 0, NaN, formResults.exitThreshold / 100, formResults.numPeriods, formResults.standardDevMult));

        break;
      }

      case "PB": {
        this.service.createTrader(new PriceBreakout
          (0, formResults.stock, formResults.size, true, false, [], 0, NaN, formResults.exitThreshold / 100, formResults.numPeriods ))

          break;
      }

      default:
        throw "Not a viable strategy";
    }



  }

}
