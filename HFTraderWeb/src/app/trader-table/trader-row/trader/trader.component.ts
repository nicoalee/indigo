import { Component, OnInit } from '@angular/core';
import { Trader } from 'src/app/models/trader';
import { TraderService } from 'src/app/services/trader-service';
import { ActivatedRoute, Route } from '@angular/router';
import { MatAccordion, MatExpansionPanel, MatFormField } from "@angular/material";

@Component({
  selector: 'app-trader',
  templateUrl: './trader.component.html',
  styleUrls: ['./trader.component.css']
})
export class TraderComponent implements OnInit {

  trader: Trader
  positions: any
  dataSource: Array<Trader>

  constructor(private traderService: TraderService, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params
      .subscribe((data) => {
        let traderID = data["id"]
        
        this.traderService.getTrader(traderID)
          .then((data: Trader) => {
            this.trader = data
            this.positions = data.positions
          })
      })      
  }

}
