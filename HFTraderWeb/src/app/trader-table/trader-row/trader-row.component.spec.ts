import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TraderRowComponent } from './trader-row.component';

describe('TraderRowComponent', () => {
  let component: TraderRowComponent;
  let fixture: ComponentFixture<TraderRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraderRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TraderRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
