import { Component, OnInit, Input } from '@angular/core';
import { Trader } from 'src/app/models/trader';
import { TraderService } from 'src/app/services/trader-service';

@Component({
  selector: '[app-trader-row]',
  templateUrl: './trader-row.component.html',
  styleUrls: ['./trader-row.component.css']
})
export class TraderRowComponent implements OnInit {

  @Input() trader: Trader
  loading:boolean = false

  constructor(private service: TraderService) { }

  onButtonClick(event) {
    event.stopPropagation()
    this.loading = true
    
    this.startOrStop(event, false)
    
  }

  ngOnInit() {

    console.log(this.trader);
    
    
    if(this.trader.stopping == true) {
      this.loading = true
    } else {
      this.loading = false
    }
    
  }

  
  /**
    * Helper to format times in mm:ss format.
    */
   minSec(millis: number): string {
    let seconds = millis / 1000;
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    const pad = seconds < 10 ? "0" : "";
    return "" + minutes + ":" + pad + seconds;
  }

  /**
   * Helper to derive a label for the trader's state: "Started",
   * "Stopped", or, if deactivated but still closing out a position,
   * "Stopping".
   */
  getState(trader: Trader) {
    return trader.active ? "Started" : "Stopped";
  }

  /**
   * Finds the trader at the given table index and uses the
   * TraderService component to send an HTTP request to toggle the
   * trader's state.
   */
  startOrStop(ev: any, hardStop: boolean) {
    this.service.setActive(this.trader.ID, !this.trader.active);
  }

}
