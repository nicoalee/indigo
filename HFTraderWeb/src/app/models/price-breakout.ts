import { Trader } from './trader';
import { Position } from "./position";

/**
 * @author Nicholas Lee
 * 
 */

 export class PriceBreakout extends Trader {
     
    periodsPerCycle:    number
    exitThreshold:      number
    
  constructor(ID: number, stock: string, size: number, active: boolean,
    stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
    exitThreshold: number, numPeriods: number) {
  
      super("PB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
      this.periodsPerCycle = numPeriods
      this.exitThreshold = exitThreshold
    }

 }
