import { Position } from "./position";

/**
 * Serializable/deserializable encapsulation of a strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
export class Trader {
  ID: number;
  stock: string;
  size: number;
  active: boolean;
  stopping: boolean;
  positions: Array<Position>;
  profitOrLoss: number;
  ROI: number;
  trades: number;

  /**
   * Store all of the given information as properties,
   * and pre-compute the total number of trades for use by the HTML template.
   */
  constructor(typeName: string, ID: number, stock: string, size: number,
      active: boolean, stopping: boolean, positions: Array<Position>,
      profitOrLoss: number, ROI: number) {

    this["@type"] = typeName
    this.ID = ID;
    this.stock = stock;
    this.size = size;
    this.active = active;
    this.stopping = stopping;
    this.positions = positions;
    this.profitOrLoss = profitOrLoss;
    this.ROI = ROI;

    this.trades = positions.length * 2;
    if (positions.length !== 0 && !positions[positions.length - 1].closingTrade) {
      this.trades -= 1;
    }
  }
}
