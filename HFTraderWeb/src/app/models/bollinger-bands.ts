import { Trader } from './trader';
import { Position } from "./position";

/**
 * @author Nicholas Lee
 * 
 */

 export class BollingerBands extends Trader {
     
    windowSize:         number
    exitThreshold:      number
    stdevMultiple:      number

    
  constructor(ID: number, stock: string, size: number, active: boolean,
    stopping: boolean, positions: Array<Position>, profitOrLoss: number, ROI: number,
    exitThreshold: number, numPeriods: number, standardDevMult: number) {
  
      super("BB", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
      this.exitThreshold = exitThreshold;
      this.windowSize = numPeriods;
      this.stdevMultiple = standardDevMult
}

 }
