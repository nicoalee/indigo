import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TraderTable } from './trader-table/trader-table';
import { WelcomeComponent } from './welcome/welcome.component';
import { HelpComponent } from './help/help.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { CreateTraderComponent } from './trader-table/create-trader/create-trader.component';

// Angular Materials modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from "@angular/material";
import { MatTableModule } from "@angular/material";
import { MatFormFieldModule } from "@angular/material";
import { MatSelectModule } from "@angular/material";
import { MatInputModule } from "@angular/material";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';
import { TraderRowComponent } from './trader-table/trader-row/trader-row.component';
import { TraderComponent } from './trader-table/trader-row/trader/trader.component';
import { MatExpansionModule } from "@angular/material";
import { MatAutocompleteModule } from "@angular/material";
import { SummaryChartComponent } from './trader-table/summary-chart/summary-chart.component';

import { ChartsModule } from "ng2-charts";

@NgModule({
  declarations: [
    AppComponent,
    TraderTable,
    WelcomeComponent,
    HelpComponent,
    NotFoundComponent,
    CreateTraderComponent,
    TraderRowComponent,
    TraderComponent,
    SummaryChartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatExpansionModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    HttpClientModule,
    ChartsModule
  ],
  entryComponents: [
    CreateTraderComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
