import { Component } from "@angular/core";
import { TraderTable } from "./trader-table/trader-table";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "Automated Trading Platform";
}
