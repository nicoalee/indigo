import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { HelpComponent } from './help/help.component';
import { TraderTable } from './trader-table/trader-table';
import { NotFoundComponent } from './not-found/not-found.component';
import { TraderRowComponent } from './trader-table/trader-row/trader-row.component';
import { TraderComponent } from './trader-table/trader-row/trader/trader.component';

const routes: Routes = [
  {path: "", redirectTo: 'welcome', pathMatch: "full"},
  {path: "welcome", component: WelcomeComponent},
  {path: "help", component: HelpComponent},
  {path: "traders", component: TraderTable},
  {path: "traders/:id", component: TraderComponent},
  {path: "**", component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
