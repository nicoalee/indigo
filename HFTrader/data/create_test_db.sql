CREATE SCHEMA TradesTest;
SET SCHEMA TradesTest;

CREATE TABLE trade
(
  ID INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1),
  when TIMESTAMP NOT NULL,
  stock VARCHAR(8) NOT NULL,
  buy BOOLEAN NOT NULL,
  size INT NOT NULL,
  price DOUBLE NOT NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE id_sequences
(
  sequence_name VARCHAR(30),
  sequence_value INTEGER,
  PRIMARY KEY (sequence_name)
);

CREATE TABLE strategy
(
  ID INT NOT NULL,
  strategy_type char(1) NOT NULL,
  stock VARCHAR(8) NOT NULL,
  size INT NOT NULL,
  active BOOLEAN NOT NULL,
  stopping BOOLEAN NOT NULL,
  started_trading TIMESTAMP,
  stopped_trading TIMESTAMP,
  PRIMARY KEY (ID)
);

CREATE TABLE two_moving_averages
(
  ID INT NOT NULL,
  length_long INT NOT NULL,
  length_short INT NOT NULL,
  exit_threshold DOUBLE NOT NULL,
  PRIMARY KEY (ID),
  CONSTRAINT two_moving_averages_strategy
    FOREIGN KEY (ID) REFERENCES strategy(ID)
);

CREATE TABLE bollinger_bands
(
  ID INT NOT NULL,
  window_size INT NOT NULL,
  stdev_multiple DOUBLE NOT NULL,
  exit_threshold DOUBLE NOT NULL,
  PRIMARY KEY (ID),
  CONSTRAINT bollinger_bands_strategy
    FOREIGN KEY (ID) REFERENCES strategy(ID)
);

CREATE TABLE price_breakout
(
  ID INT NOT NULL,
  periods_per_cycle INT NOT NULL,
  exit_threshold DOUBLE NOT NULL,
  PRIMARY KEY (ID),
  CONSTRAINT price_breakout_strategy
    FOREIGN KEY (ID) REFERENCES strategy(ID)
);

CREATE TABLE position
(
  ID INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1),
  strategy_ID INT NOT NULL,
  opening_trade_ID INT NOT NULL,
  closing_trade_ID INT,
  PRIMARY KEY (ID),
  CONSTRAINT position_strategy
    FOREIGN KEY (strategy_ID) REFERENCES strategy (ID),
  CONSTRAINT position_trade_1
    FOREIGN KEY (opening_trade_ID) REFERENCES trade (ID),
  CONSTRAINT position_trade_2
    FOREIGN KEY (closing_trade_ID) REFERENCES trade (ID)
);

CREATE TABLE price_point
(
  ID INT NOT NULL GENERATED ALWAYS AS IDENTITY(START WITH 1),
  stock VARCHAR(8) NOT NULL,
  when TIMESTAMP NOT NULL,
  opn DOUBLE NOT NULL,
  high DOUBLE NOT NULL,
  low DOUBLE NOT NULL,
  cls DOUBLE NOT NULL,
  volume INTEGER NOT NULL,
  PRIMARY KEY (ID)
);

INSERT INTO id_sequences VALUES ('strategies', 1);

