package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Represents a price-breakout trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Edward Y. Liu
 */
@Entity
@DiscriminatorValue("C")
public class PriceBreakout extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int periodsPerCycle;
	private double exitThreshold;

	public PriceBreakout() {
	}
	
	public PriceBreakout(String stock, int size, int periodPerCycle, double exitThreshold) {
		super(stock, size);
		this.periodsPerCycle = periodPerCycle;
		this.exitThreshold = exitThreshold;
	}
	
	public int getPeriodsPerCycle() {
		return this.periodsPerCycle;
	}

	public void setPeriodsPerCycle(int period) {
		this.periodsPerCycle = period;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}

	@Override
	public String toString() {
		return String.format("PriceBreakout: [period=%d, exit=%1.4f, %s]",
				periodsPerCycle, exitThreshold, stringRepresentation());
	}
}
