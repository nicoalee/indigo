package com.citi.trading.strategy;

import com.citi.trading.Trade;

/**
 * Persistence service to coordinate recording of positions and trades.
 * 
 * @author Will Provost
 */
public interface StrategyPersistence {
	
	/**
	 * Create a new position for the given strategy, with the given opening trade.
	 */
	public Position open(Strategy strategy, Trade openingTrade);

	/**
	 * Close the currently open position, with the given closing trade.
	 */
	public Position close(Strategy strategy, Trade closingTrade);

	/**
	 * Split the currently open position, in case of a partial fill. 
	 * This means (a) reduce the size of the opening trade to match the share 
	 * count of the closing trade, (b) record the closing trade, and
	 * (c) open a new position representing the unfilled balance, so that
	 * we will continue to look to close the rest of the position. 
	 */
	public Position splitAndClosePart(Strategy strategy, Trade closingTrade);
	
	/**
	 * Set the trading start time for the strategy to current system time, 
	 * and wipe any previous stop time to null.  
	 */
	public Strategy startTrading(Strategy strategy);
	
	/**
	 * Set the trading stop time for the strategy to current system time.  
	 */
	public Strategy stopTrading(Strategy strategy);

	public Strategy setStoppingActive(Strategy strategy, boolean b, boolean a);
}
