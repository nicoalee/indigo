package com.citi.trading.strategy;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

/**
 * Implementation of the price-breakout trading strategy.
 * We record the open, close, high and low of a stock for a period of time
 * and track whether the preceding high/low fit inside the previous period's
 * open/close - narrow. A trade is triggered if either the high or low leaves
 * the open/close range. Open long if the latest close is higher than the open, 
 * short if lower. Once open, we close on a configurable percentage
 * gain or loss.
 *
 * @author Edward Y. Liu
 */
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PriceBreakoutTrader extends Trader<PriceBreakout> {
	private static final Logger LOGGER =
	        Logger.getLogger(PriceBreakoutTrader.class.getName ());
	
	private int period = 0;
	private double prevOpen;
	private double prevClose;
	
	private boolean prevIsNarrow;
	private boolean isNarrow = false;
	private boolean isBreakout;
	private boolean closeIsHigher;
	
	public PriceBreakoutTrader(PricingSource pricing, 
			OrderPlacer market, StrategyPersistence strategyPersistence) {
    	super(pricing, market, strategyPersistence);
    }
	
	public int getNumberOfPeriodsToWatch() {
		return strategy.getPeriodsPerCycle();
	}
	
	/**
     * Helper method to update our flags: is it getting narrow? is there a
     * breakout? if so, was the previous close higher than the current open?
     */
	private void checkBreakout(double prevOpen, double prevClose, double current) {
		
		prevIsNarrow = isNarrow;
		if (prevOpen > prevClose) {
			isNarrow = (prevOpen > current) && (current > prevClose);
			
		} else {
			isNarrow = (prevClose > current) && (current > prevOpen);
		}
		
		isBreakout = prevIsNarrow && !isNarrow;
		closeIsHigher = prevClose > current;
	}
	
	protected void handleDataWhenOpen(PriceData data) {
		double currentPrice = data.getData(1).findAny().get().getClose();
		double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
		double profitOrLoss = currentPrice / openingPrice - 1.0;
    	
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(currentPrice);
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}
	}
	
	protected void handleDataWhenClosed(PriceData data) {
		
		if (tracking.get()) {
			double curr = data.getData(1).findAny().get().getClose();
			checkBreakout(prevOpen, prevClose, curr);	
			
			if (isBreakout) {
				opener.placeOrder(closeIsHigher, curr);
			}
			
			if (period % getNumberOfPeriodsToWatch() == 0) {
				period = 0;
				
				prevOpen = prevClose;
				prevClose = curr;
			}
			
		} else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
			
			period = 0;
			
			List<PricePoint> prices = data.getData(getNumberOfPeriodsToWatch()).collect(Collectors.toList());
			prevOpen = prices.get(0).getClose();
			prevClose = prices.get(getNumberOfPeriodsToWatch() - 1).getClose();	
			
			tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data: the open and close of the previous period.");
		}
		
		period++;
	}
}
