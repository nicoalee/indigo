package com.citi.trading.strategy;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.logging.Logger;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.Trade.Result;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricingSource;

/**
 * Base implementation for trading logic. Any trader has a {@link Strategy}
 * that provides the parameters for the specific type of trader --
 * e.g. a two-moving-averages trader implements the logic of the 2MA 
 * algorithm, actively reading pricing data, placing trader orders, etc.,
 * and knows its stock, size, short and long average lengths, and exit 
 * threshold by consulting the associated two-moving-averages strategy object.
 * Strategies are persistent and stateful; traders are transient and
 * behavioral, consulting the strategies for persistent state and, it's true,
 * holding some transient state of their own.
 * 
 * This base class knows how to interact with the pricing service and the
 * mock market, and how to record positions and trades when they happen.
 * It does not know how to decide when to buy or sell, and it looks to 
 * derived classes to do that.
 * 
 * @author Will Provost
 */
public abstract class Trader<S extends Strategy> implements Consumer<PriceData>
{
    private static final Logger LOGGER = Logger.getLogger(Trader.class.getName ());

    protected S strategy;
    protected AtomicBoolean tracking = new AtomicBoolean(false);
    
    protected PricingSource pricing;
    protected OrderPlacer market;
    protected StrategyPersistence strategyPersistence;
    
    //@Autowired
    //protected StrategyRepository strategyRepository = null;

    /**
     * Provide the three big externalities -- pricing feed, mock market, and
     * persistence provider -- as constructor arguments in an ordinary Java 
     * <strong>new</strong> call, or when created as a Spring bean these will
     * all be injected to the constructor. 
     */
    public Trader(PricingSource pricing, OrderPlacer market, 
    		StrategyPersistence strategyPersistence) {
    	this.pricing = pricing;
    	this.market = market;
    	this.strategyPersistence = strategyPersistence;
    }
    
    public S getStrategy() {
    	return strategy;
    }
    
    public void setStrategy(S strategy) {
    	this.strategy = strategy;
    }
    
    /**
     * Since the strategy can't be dependency-injected (it's brought up
     * from the database during the run of the application), this helper 
     * method enforces a rule that the strategy must be set before 
     * starting any operations.
     * 
     * @throws IllegalStateException If the strategy is null
     */
    protected void checkStrategy() {
    	if (strategy == null) {
    		throw new IllegalStateException
    			("You must set a strategy object, so I know what to do.");
    	}
    }
    
    public boolean isTracking() {
    	return tracking.get();
    }
    
    /**
     * @return True if the strategy has an open position.
     */
    public boolean isOpen() {
    	checkStrategy();
    	return strategy.getOpenPosition() != null;
    }
    
    /**
     * Derived classes inform us as to how many periods of pricing data  
     * they will need to check as part of their decision-making.
     */
    public abstract int getNumberOfPeriodsToWatch();
    
    /**
     * Derived classes implement this to make their trading decisions;
     * called whenever the trader has closed its most recent position and is
     * on the hunt for a new one.
     */
    protected abstract void handleDataWhenOpen(PriceData data);

    /**
     * Derived classes implement this to make their trading decisions;
     * called whenever the trader has an open position.
     */
    protected abstract void handleDataWhenClosed(PriceData data);
    
    /**
     * Subscribe to the pricing service.
     */
	public synchronized void startTrading() {
    	checkStrategy();
    	    	
    	pricing.subscribe(strategy.getStock(), getNumberOfPeriodsToWatch(), this);
    	tracking.set(false);
   		strategy.setActive(true);
    	LOGGER.info("Trader " + strategy.getId() + " has started trading.");
    }
    
    /**
     * Unsubscribe from the pricing service.
     * @throws InterruptedException 
     */
	public synchronized void stopTrading() {
    	checkStrategy();
    	if(isOpen()) {
			strategy = (S)strategyPersistence.setStoppingActive(strategy, true, true);
			LOGGER.info("Trader " + strategy.getId() + " stopping " + strategy.isStopping());
    	} else {
    		System.out.println(strategy);
    		strategy = (S)strategyPersistence.setStoppingActive(strategy, false, false);
			System.out.println(strategy);
			pricing.unsubscribe(strategy.getStock(), this);
	    	LOGGER.info("Trader " + strategy.getId() + " has stopped trading.");
	    	strategy.setActive(false);
    	}
    }
    
    /**
     * Pass the pricing notification to one of the two handler methods
     * implemented by the derived class.
     */
    public synchronized void accept(PriceData data) {
    	if (isOpen()) {
    		handleDataWhenOpen(data);
    	} else {
    		handleDataWhenClosed(data);
    	}
    }
    
    /**
     * Derived classes use the instance of this class to make trade orders.
     */
    protected class Opener implements Consumer<Trade> {
    	
    	/**
    	 * Creates a {@link Trade} representing our offer to buy or sell stock,
    	 * synthesizing the strategy's stock and size with the given buy/sell flag
    	 * and offer price. Sends this to the mock market, with ourselves
    	 * (the {@link Trader#opener} instance) as the callback.
    	 */
    	public void placeOrder(boolean buy, double price) {
    		checkStrategy();
    		if (isOpen()) {
    			throw new IllegalStateException("We already have an open position.");
    		}
    		
    		Trade trade = new Trade
    				(strategy.getStock(), buy, strategy.getSize(), price);
    		market.placeOrder(trade, this);
    	}
    	
    	/**
    	 * Checks the results field of the notification. If the trade was fully filled,
    	 * record as a new, open position. If partially filled, record that as a smaller
    	 * open position and live with the fact that we're investing less than our maximum.
    	 * If rejected, don't do anything: we're back on the hunt for an opening signal.
    	 */
    	public void accept(Trade notification) {
    		if(notification.getResult() == Result.FILLED) {
    			strategyPersistence.open(strategy, notification);
    			LOGGER.info("Trader " + strategy.getId() + " has opened: " + notification);
    		} else if(notification.getResult() == Result.PARTIALLY_FILLED) {
    			strategyPersistence.open(strategy, notification);
    			LOGGER.info("Trader "+ strategy.getId() + " has Partially opened " + notification);
    		} else if(notification.getResult() == Result.REJECTED) {
        		LOGGER.info("Trader " + strategy.getId() + " has been rejected while opening " + notification);
    		}
    	}
    }
    protected Opener opener = new Opener();
    
    /**
     * Derived classes use the instance of this class to make trade orders.
     */
    protected class Closer implements Consumer<Trade> {
    	
    	/**
    	 * Creates a {@link Trade} representing our offer to buy or sell stock,
    	 * symmetrical to our opening trade but with the given offer price.
    	 * Sends this to the mock market, with ourselves
    	 * (the {@link Trader#closer} instance) as the callback.
    	 */
    	public void placeOrder(double price) {
    		checkStrategy();
    		Position position = strategy.getOpenPosition();
    		if (strategy.getOpenPosition() == null) {
    			throw new IllegalStateException("We don't have an open position to close.");
    		}

    		Trade trade = new Trade(strategy.getStock(), 
    				!position.getOpeningTrade().isBuy(), 
    				position.getOpeningTrade().getSize(), price);
    		market.placeOrder(trade, this);
    	}
    	
    	/**
    	 * Checks the results field of the notification. If the trade was fully filled,
    	 * close the current position. If partially filled, split the current position.
    	 * If rejected, don't do anything: we'll continue to look for signals to close,
    	 * but for example if the price falls back inside our threshold we may wait
    	 * a while longer.
    	 */
    	public void accept(Trade notification) {
    		if(notification.getResult() == Result.FILLED) {
	    		strategyPersistence.close(strategy, notification);
	    		LOGGER.info("Trader " + strategy.getId() + " has closed: " + notification);
	    		LOGGER.info("Trader " + strategy.getId() + " stopping variable: " +strategy.isStopping());
	    		if(strategy.isStopping()) {
					stopTrading();
	    			LOGGER.info("Trader " + strategy.getId() + " stopping variable: " + strategy.isStopping());
	    		}
    		} else if(notification.getResult() == Result.PARTIALLY_FILLED) {
    			strategyPersistence.splitAndClosePart(strategy, notification);
    			LOGGER.info("Trader "+ strategy.getId() + " has Partially Closed " + notification);
    		} else if(notification.getResult() == Result.REJECTED) {
    			LOGGER.info("Trader " + strategy.getId() + " has been rejected while closing " + notification);
    		}
    	}
    }
    protected Closer closer = new Closer();
    
    @Override
    public String toString() {
    	return String.format("Trader<%s>", strategy);
    }
}
