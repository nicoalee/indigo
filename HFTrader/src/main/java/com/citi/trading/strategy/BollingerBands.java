package com.citi.trading.strategy;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;


/**
 * Represents a bollinger-bands trading strategy, adding the parameters
 * that are specific to that algorithm.
 * 
 * @author Edward Y. Liu
 */
@Entity
@DiscriminatorValue("B")
public class BollingerBands extends Strategy implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int windowSize;		// number of periods
	private double stdevMultiple;
	private double exitThreshold;

	public BollingerBands() { }
	
	public BollingerBands(String stock, int size, int windowSize, double stdevMultiple, double exitThreshold) {
		super(stock, size);
		this.windowSize = windowSize;
		this.stdevMultiple = (double) stdevMultiple;
		this.exitThreshold = (double) exitThreshold;
	}

	public int getWindowSize() {
		return this.windowSize;
	}

	public void setWindowSize(int windowSize) {
		this.windowSize = windowSize;
	}
	
	public double getStdevMultiple() {
		return this.stdevMultiple;
	}

	public void setStdevMultiple(double stdevMultiple) {
		this.stdevMultiple = stdevMultiple;
	}

	public double getExitThreshold() {
		return this.exitThreshold;
	}

	public void setExitThreshold(double exitThreshold) {
		this.exitThreshold = exitThreshold;
	}
	
	@Override
	public String toString() {
		return String.format("BollingerBands: [window size=%d, exit=%1.4f of stdev which is %1.4f, %s]",
				windowSize, exitThreshold,stdevMultiple, stringRepresentation());
	}
}
