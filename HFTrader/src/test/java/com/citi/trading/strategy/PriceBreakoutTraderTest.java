package com.citi.trading.strategy;

import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.sql.Timestamp;
import java.util.function.Consumer;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;
import com.citi.trading.strategy.TraderTest.TraderMocks;

/**
 * Unit test for the {@link PriceBreakoutTrader}. We configure mock
 * pricing, market, and persistence, and aggressively verify outbound calls
 * as a way to check the trading behavior of the component.
 * 
 * @author Edward Y. Liu
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=PriceBreakoutTraderTest.Config.class)
@DirtiesContext(classMode=ClassMode.AFTER_EACH_TEST_METHOD)
public class PriceBreakoutTraderTest {
	public static final String STOCK = "AA";
	public static final int SIZE = 100;
	public static final double PRICE = 100.0;
	
	@Configuration
	@Import(TraderMocks.class)
	public static class Config {
		
		@Bean
		public PriceBreakoutTrader littlePBTrader(PricingSource pricing, 
				OrderPlacer market, StrategyPersistence strategyPersistence) {
			PriceBreakout PB = 
					new PriceBreakout(STOCK, SIZE, 3, 0.03);
			PriceBreakoutTrader trader = 
					new PriceBreakoutTrader(pricing, market, strategyPersistence); 
			trader.setStrategy(PB);
			return trader;
		}

		@Bean
		public PriceBreakoutTrader biggerPBTrader(PricingSource pricing, 
				OrderPlacer market, StrategyPersistence strategyPersistence) {
			PriceBreakout PB = 
					new PriceBreakout("MRK", SIZE, 5, 0.03);
			PriceBreakoutTrader trader = 
					new PriceBreakoutTrader(pricing, market, strategyPersistence); 
			trader.setStrategy(PB);
			return trader;
		}
	}
	
	@Resource(name="littlePBTrader")
	private PriceBreakoutTrader littleTrader;
	
	@Resource(name="biggerPBTrader")
	private PriceBreakoutTrader biggerTrader;
	
	@Autowired
	private OrderPlacer mockMarket;
	
	private Trade latestTrade;
	private Consumer<Trade> latestSubscriber;
	
	/**
	 * Automatically and immediately confirm any requested trade in full.
	 */
	@Before
	@SuppressWarnings("unchecked") // Consumer<Trade> from Object
	public void setUp() {
		doAnswer(inv -> {
				latestTrade = (Trade) inv.getArgument(0);
				latestTrade.setResult(Trade.Result.FILLED);
				latestSubscriber = (Consumer<Trade>) inv.getArgument(1);
				return null;
			}).when(mockMarket).placeOrder(any(), any());		
	}
	
	public static void addPricePoint(PriceData data, double price) {
		Timestamp time = data.getLatestTimestamp();
		PricePoint point = new PricePoint(new Timestamp(time.getTime() + 15000), 
				price, price, price, price, 100);
		data.addData(point);
	}
	
	public static PriceData createPriceData(double... prices) {
		PricePoint[] points = new PricePoint[prices.length];
		Timestamp time = new Timestamp(1546351200000L); // Jan 1 2019, 9:00 AM
		int index = 0;
		for (double price : prices) {
			points[index++] = new PricePoint(time, price, price, price, price, 100);
			time = new Timestamp(time.getTime() + 15000);
		}
		PriceData data = new PriceData("", prices.length);
		data.addData(points);
		return data;
	}
	
	private void confirmAndRecordRequestedTrade(PriceBreakoutTrader trader) {
		latestSubscriber.accept(latestTrade);
		if (trader.isOpen()) {
			trader.getStrategy().getOpenPosition().setClosingTrade(latestTrade);
		} else {
			Position position = new Position(trader.getStrategy(), latestTrade);
			trader.getStrategy().addPosition(position);
		}
	}
	
	
	@Test
	public void testLong() {
		PriceData data = createPriceData(110, 100, 90);
		littleTrader.accept(data);
		
		addPricePoint(data, 102);
		littleTrader.accept(data);
		addPricePoint(data, 85);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
				.and(hasProperty("price", closeTo(85, 0.0001)))), any());
		confirmAndRecordRequestedTrade(littleTrader);
		
		addPricePoint(data, 92);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
				.and(hasProperty("price", closeTo(92, 0.0001)))), any());
		confirmAndRecordRequestedTrade(littleTrader);
		
		assertThat(littleTrader.getStrategy().getROI(), closeTo(0.0823, 0.001));
		assertThat(littleTrader.getStrategy().getProfitOrLoss(), closeTo(700.0, 0.001));
	}
	
	@Test
	public void testShort() {
		PriceData data = createPriceData(110, 100, 90);
		littleTrader.accept(data);
		
		addPricePoint(data, 102);
		littleTrader.accept(data);
		addPricePoint(data, 111);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(false)))
				.and(hasProperty("price", closeTo(111, 0.0001)))), any());
		confirmAndRecordRequestedTrade(littleTrader);
		
		addPricePoint(data, 98);
		littleTrader.accept(data);
		verify(mockMarket).placeOrder((Trade) argThat(both(hasProperty("buy", equalTo(true)))
				.and(hasProperty("price", closeTo(98, 0.0001)))), any());
		confirmAndRecordRequestedTrade(littleTrader);
		
		assertThat(littleTrader.getStrategy().getROI(), closeTo(0.117, 0.001));
		assertThat(littleTrader.getStrategy().getProfitOrLoss(), closeTo(1300.0, 0.001));
	}
}
