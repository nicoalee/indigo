package com.citi.trading.strategy;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.citi.trading.TestDB;
import com.citi.trading.Trade;
import com.citi.trading.WebExceptions;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This is essentially an integration test of the {@link TraderService},
 * but because it uses Spring's <strong>MockMvc</strong> facility it is
 * totally isolated and can be run along with unit tests, so we vary from
 * our "*IntegrationTest" naming convention. Test cases cover all of the HTTP
 * operations, including error cases such as no such strader and conflict
 * on creating a trader. We use a mock {@link ActiveTraders} in order to
 * verify back-end operations. 
 * 
 * @author Will Provost
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=TraderServiceMockMvcTest.Config.class)
public class TraderServiceMockMvcTest {

	@Configuration
	@EnableAutoConfiguration
	@EntityScan(basePackageClasses=Trade.class)
	@EnableJpaRepositories(basePackageClasses=Trade.class)
	@PropertySource("classpath:memoryDB.properties")
	public static class Config {
		
		@Bean
		public TestDB testDB() {
			return new TestDB();
		}
		
		@Bean
		public ActiveTraders mockActiveTraders() {
			return mock(ActiveTraders.class);
		}
		
		@Bean
		public TraderService traderService() {
			return new TraderService();
		}
		
		@Bean
		public WebExceptions webExceptions() {
			return new WebExceptions();
		}
	}
	
	@Autowired
	private TestDB testDB;
	
	@Autowired
	private ActiveTraders mockActiveTraders;
	
	@Autowired
	private StrategyRepository strategyRepository;
	
    @Autowired
    private WebApplicationContext context;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    private MockMvc mockMVC;
    
    /**
    Initialize the mock MVC system using an injected web-application context.
    */
    @Before
    public void setUp () throws Exception {
		testDB.reset();
        mockMVC = MockMvcBuilders.webAppContextSetup (context).build ();
    }
    
	@Test
	public void testGetTraders() throws Exception {
		mockMVC.perform(get("/traders"))
			.andExpect(status().isOk())
			.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$[0].stock").value("MRK"))
			.andExpect(jsonPath("$[1].stock").value("IBM"));
	}
	
	@Test
	public void testGetTraderByID() throws Exception {
		mockMVC.perform(get("/traders/1"))
			.andExpect(status().isOk())
			.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.@type").value("2MA"));
	}
	
	@Test
	public void testGetTraderByIDNotFound() throws Exception {
		mockMVC.perform(get("/traders/55")).andExpect(status().isNotFound());
	}
	
	@Test
	@DirtiesContext
	public void testDeactivateAndReactivateTrader() throws Exception {

		mockMVC.perform(put("/traders/1/active").content("false"))
		.andExpect(status().isOk());
		Strategy strategy = strategyRepository.findById(1).get();
		assertThat(strategy.isActive(), equalTo(true));
		verify(mockActiveTraders).removeTraderFor(any());

		mockMVC.perform(put("/traders/1/active").content("true"))
			.andExpect(status().isOk());
		strategy = strategyRepository.findById(1).get();
		assertThat(strategy.isActive(), equalTo(true));
		verify(mockActiveTraders).addTraderFor(any());
	}
	
	@Test
	public void testActivateTraderNotFound() throws Exception {
		mockMVC.perform(put("/traders/55/active").contentType(MediaType.APPLICATION_JSON)
				.content("{ \"start\": true, \"hard\": false }"))
			.andExpect(status().isNotFound());
	}
	
	@Test
	@DirtiesContext
	public void testSaveStrategy() throws Exception {
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		mockMVC.perform(post("/traders").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(twoMA)))
			.andExpect(status().isCreated())
			.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
			.andExpect(jsonPath("$.id").hasJsonPath());
		verify(mockActiveTraders).addTraderFor(any());
	}
	
	@Test
	public void testSaveStrategyConflict() throws Exception {
		TwoMovingAverages twoMA = 
				new TwoMovingAverages("AA", 100, 30000, 60000, 0.03);
		twoMA.setId(11);
		mockMVC.perform(post("/traders").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(twoMA)))
			.andExpect(status().isConflict());
	}
}
