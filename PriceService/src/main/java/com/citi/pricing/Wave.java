package com.citi.pricing;

public class Wave
{
    private double amplitude;
    private int period;
    private int offset;
    private int pause;

    public Wave(double amplitude, int period, int pause, int offset) {
        this.amplitude = amplitude;
        this.period = period;
        this.pause = pause;
        this.offset = offset;
    }
    
    public double getNextImpact() {
        
        offset = (offset + 1) % (period + pause);
        
        if (offset < period) {
            return amplitude * Math.sin(offset * Math.PI * 2 / period);
        } else {
            return 0;
        }
    }
    
    public static void main(String[] args) {
        Wave wave = new Wave(.05, 10, 0, 0);
        for (int x = 0; x < 10; ++x) {
            System.out.println(wave.getNextImpact ());
        }
    }
}
